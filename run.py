#!/usr/bin/python
import socket, sys,readline
from time import gmtime, strftime
from sys import platform
from cryptography.fernet import Fernet
from optparse import OptionParser
def __banner__():
  print("""  __     ___                                 
 |  |--.'  _.--------.-----.-----.-----.----.
 |  _  |   _|        |__ --|  _  |  -__|   _|
 |_____|__| |__|__|__|_____|___  |_____|__|  
                           |_____|
                                  Server service !""")
class color:
	P    =  '\033[95m' # purple
	B    =  '\033[94m' # Blue
	BOLD =  '\033[1m'  # Bold
	G    =  '\033[92m' # Green
	Y    =  '\033[93m' # Yellow
	R    =  '\033[91m' # Red
	W    =  '\033[97m' # White
	BL   =  '\033[90m' # Black
	M    =  '\033[95m' # Magenta
	C    =  '\033[96m' # Cyan
	ENDC =  '\033[0m'  # end colors
	if sys.platform == 'win32':
		P    =  '' # purple
		B    =  '' # Blue
		BOLD =  '' # Bold
		G    =  '' # Green
		Y    =  '' # Yellow
		R    =  '' # Red
		W    =  '' # White
		BL   =  '' # Black
		M    =  '' # Magenta
		C    =  '' # Cyan
		ENDC =  '' # end colors

class UDP_SERVER:
	"""
	RUNNING SERVER & GET MSG VIA UDP PORT 
	"""
	def DECRYPTION(self,msg):
		f = Fernet("8gqHlImW7YSq7CV5gfzTo0R4c-wdr4TikBl_aQs6_ZY=")
		crack =	f.decrypt(msg)
		return crack
	def BIND(self, ip, port):
		ip,port = str(ip),int(port)
		print ("UDP IP   :", ip)
		print ("UDP PORT :", port)
		sock = socket.socket(socket.AF_INET,
			socket.SOCK_DGRAM)
		sock.bind((ip, port))
		while True:
			time = strftime("%H:%M:%S", gmtime())
			time = "["+color.G+time+color.ENDC+"]"
			data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
			ip,p=addr[0], addr[1]
			msg_dec=self.DECRYPTION(data).decode('utf-8')
			print(time+"["+color.G+ip+color.ENDC+"] "+msg_dec)

	def __init__(self):pass

try:
	print("\033c")
	__banner__()
	udp=UDP_SERVER()
	#tcp=TCP_SERVER()
	for arg in sys.argv:
		if (arg=="udp"):
			parser = OptionParser()
			parser.add_option("--ip",type=str,
				help="IP SERVER")
			parser.add_option("--port","-p",type=int,
				help="PORT")
			(options,args) = parser.parse_args()
			ip=options.ip
			port=options.port
			if ip and port:
				udp.BIND(ip, port)
		if (arg=="tcp"):
			print("SOON")
		if (arg=="--help" or arg=="-h"):
			print("usage : python "+sys.argv[0]+" --ip [IP] -p/--port [PORT]")
except KeyboardInterrupt:
	print("\nExit Now")
#except OSError:
	#print("Error, ")