#!/usr/bin/python
import socket, sys,readline
from cryptography.fernet import Fernet
from optparse import OptionParser
from time import gmtime, strftime
from sys import platform
def __banner__():
  print("""  __     ___                                 
 |  |--.'  _.--------.-----.-----.-----.----.
 |  _  |   _|        |__ --|  _  |  -__|   _|
 |_____|__| |__|__|__|_____|___  |_____|__|  
                           |_____|
                                  Client service !""")
class color:
  P    =  '\033[95m' # purple
  B    =  '\033[94m' # Blue
  BOLD =  '\033[1m'  # Bold
  G    =  '\033[92m' # Green
  Y    =  '\033[93m' # Yellow
  R    =  '\033[91m' # Red
  W    =  '\033[97m' # White
  BL   =  '\033[90m' # Black
  M    =  '\033[95m' # Magenta
  C    =  '\033[96m' # Cyan
  ENDC =  '\033[0m'  # end colors
  if sys.platform == 'win32':
    P    =  '' # purple
    B    =  '' # Blue
    BOLD =  '' # Bold
    G    =  '' # Green
    Y    =  '' # Yellow
    R    =  '' # Red
    W    =  '' # White
    BL   =  '' # Black
    M    =  '' # Magenta
    C    =  '' # Cyan
    ENDC =  '' # end colors

class UDP_MSG:
  def ENCRYPTION(self, msg):
    f = Fernet("8gqHlImW7YSq7CV5gfzTo0R4c-wdr4TikBl_aQs6_ZY=")
    msg = bytes(msg, encoding="ascii")
    token = f.encrypt(msg)
    return token
  def RUN(self,ip, port,user):
    #sock = socket.socket(socket.AF_INET,
        #socket.SOCK_DGRAM)
    #print( sock.connect((ip,port)) )
    print("UDP IP  :", ip)
    print("UDP PORT:", port)
    print("USERNAME:", user)
    while True:
      time = strftime("%H:%M:%S", gmtime())
      time = "["+color.G+time+color.ENDC+"]"
      msg = input(time+" "+user+" (Me) : ")
      MESSAGE = user+" : "+msg
      msg_enc = self.ENCRYPTION(MESSAGE)
      sock = socket.socket(socket.AF_INET,
        socket.SOCK_DGRAM)
      sock.sendto(msg_enc, (ip, port))


def main():
  print("\033c")
  __banner__()
  udp=UDP_MSG()
  for arg in sys.argv:
    if (arg=="udp"):
      parser = OptionParser()
      parser.add_option("--ip",type=str,
        help="IP SERVER")
      parser.add_option("--port","-p",type=int,
        help="PORT")
      parser.add_option("--user","-u",
        help="PORT")
      (options,args) = parser.parse_args()
      ip=options.ip
      port=options.port
      user=options.user
      if ip and port and user:
        if len(user)>=6:
          print("Username must be [6 > words] !")
          sys.exit()
        else:
          udp.RUN(ip, port, user)
    if (arg=="tcp"):
      print("SOON")
    if (arg=="--help" or arg=="-h"):
      print("usage : python "+sys.argv[0]+" --ip [IP] -p/--port [PORT] -u/--user [USERNAME]")
try:
  main()
except KeyboardInterrupt:
  print("\nExit Now")
except IndexError:
  print("Error ?!\nusage : python "+sys.argv[0]+" [IP] [PORT] [USERNAME]")
